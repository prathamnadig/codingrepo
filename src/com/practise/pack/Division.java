package com.practise.pack;

import java.util.Scanner;

public class Division {

	public static void divide() {
		Scanner s = new Scanner(System.in);

		System.out.println("enter the dividend");
		int dividend = s.nextInt();
		System.out.println("enter the divisor");
		int divisor = s.nextInt();

		int quotient = 0;

		while (dividend >= divisor) {
			dividend = dividend - divisor;
			quotient++;
		}

		System.out.println("Quotient is " + quotient);

		System.out.println("Remainder is " + dividend);
	}

	public static void main(String[] args) {

		divide();

	}
}
