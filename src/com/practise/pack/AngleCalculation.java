// program to find angle between hour and minute

package com.practise.pack;

import java.util.Scanner;

public class AngleCalculation {

	public static void angleBetweenHourAndMinute(int hour, int minute) {

		int h = (hour * 360) / 12 + (minute * 360) / (12 * 60);

		int m = (minute * 360) / (60);

		int angle = Math.abs(h - m);

		if (angle > 180) {
			angle = 360 - angle;
		}

		System.out.println("The angle between hour and minute is "+angle);
	}

	public static void main(String[] args) {

		Scanner s = new Scanner(System.in);
		System.out.println("Enter hour");
		int hour = s.nextInt();
		System.out.println("enter minute");
		int minute = s.nextInt();

		angleBetweenHourAndMinute(hour, minute);
		
		System.out.println();

	}

}
